data "aws_codeartifact_repository_endpoint" "npm" {
  domain     = local.domain
  repository = local.npm_repository
  format     = "npm"
}

resource "aws_codeartifact_repository" "artifact-repository" {
  repository = "${var.owner}-${var.env}-${var.repository_name}"
  domain = local.domain
  upstream {
    repository_name = data.aws_codeartifact_repository_endpoint.npm.repository
  }
  tags = {
    Product = var.owner
    Environment = var.env
  }
}