variable "region" {}
variable "env" {}
variable "owner" {}
variable "env_tag" {}
variable "repository_name" {}
locals {
  kms_key_id = "arn:aws:kms:eu-west-1:929226109038:key/ee866c74-4272-4a57-b27f-b8bf7b3247a0"
}
locals {
  domain = "${var.owner}-${var.env}-domain"
}
locals {
  npm_repository = "node-store"
}